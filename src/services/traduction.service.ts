import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class TraductionService {
    traduction: any = {};
    constructor(
        private http : HttpClient,
    ) {
        if (localStorage.getItem('language') == 'english') {
            this.traduction = {

                //signin page

                titleWelcom: "Welcome to EasyWinz",
                placeholderEmail: "Email",
                placeholderPhone: "Mobile",
                placeholderPassword: "Password",
                rememberPassword: "Remember Password",
                forgetPassword: "Forgot Password?",
                buttonLogin: "Login",
                dontHaveAccount: "Don't have an account?",
                registerNow: "Register now",

                //signup page

                placeholderName: "Name",
                placeholderRetypePassword: "Retype Password",
                placeholderCountry: "Country",
                buttonRegister: "Register",
                alreadyHaveAnAccount: "Already have an account?",
                loginNow: "Login now",
                placeholderAddress: "Address",
                buttonSend: 'Send',
                mal: "Male",
                famel: "Female",

                //forget password

                titleReset: "Reset password",
                buttonReset: "Request Reset Code",
                DidntReceived: "Didn't Received",
                sendAgain: "Send again",

                //ver code

                textVer: " Enter Verification Code",
                buttonVer: 'Send',
                activate:"Activate",

                //profile page
                profile: "Profile",
                personalDetails: "Personal Details",
                hello: "Hello",
                activeT: "Active Tickets",
                logout: "Logout",
                setting: "Setting",
                language: "Language",
                currency: "Currency",
                general: "General",
                howItWorks: "How It Works",
                products: "Products",
                nextPrize: "Next Prize Suggestions",
                payment: "Cards",
                cards: 'Cards',
                privacyPolicy: "Privacy Policy",
                buttonCall: "Call Us",
                buttonEmail: "Email Us",
                buttonUpdate: "Update",
                alert: "Are you sure you want to logout?",
                loginAlert: "Sign in",
                signupAlert: "Sign up",
                alertMassage: "Please Login or Register",
                //cards

                placeholderCardNumber: "Credit or Debit Card Number",
                placeholderCardName: "Card Name",
                placeholderExpiry: "Expiry Date",
                placeholderCvv: "CVV",
                placeholderSelectType: "Select Type",
                saveThisCard: "Save this card for future purchases",
                buttonSendCard: "Send",
                messageLoading: "Please wait...",

                //Tickets

                tickets: "Tickets",
                ticketNumber: "TICKET NUMBER :",

                //products

                productsP: "Products",

                //prize

                prize: "Next Prize Suggestions",

                //how_it_work

                how_it_work: "How it works",

                //privacyPolicy

                //  privacyPolicy : "Privacy policy"

                //product details

                productDetails: "Product details",

                prizeDetails: "Prize Details",
                productdetails: "Product Details",
                for: "for",

                //payment

                paymentPage: 'Payment',
                payNow: "PAY NOW",
                addnewcard: "Add New Card",
                creditOrDebit: "Credit or Debit Card",
                month: "Month",
                year: "Year",



                //product details
                addToCart: "Add To Cart",


                //home

                titleHome: 'EasyWinz',
                closing: "Campaign's Closing Soon",
                getachance: "Get a chance to",
                win: "win",
                soldOf: "sold of",
                CAvailable: "Campaign's Available",
                buy: 'Buy',
                //cart
                inclusive: 'Inclusive of 5% VAT',
                grandTotal: "Grand Total",
                ProceedToPayment: 'Proceed to payment',
                titleCart: 'Cart',
                empty1: "Your Cart is Empty",
                empty2: "Add some items to cheer it up",
                textCard: 'With every you purchased you will double your chance to win',
                deletFromCart: "product deleted",
                //winners

                winners: "Winners",
                soldOut: "Sold Out",
                congratulations: "Congratulations",
                ticketNo: "Ticket No:",
                announced: "Announced:",

                //wishlist

                wishlist: "Wishlist",

            }
        }
        if (localStorage.getItem('language') == 'indian') {

            this.traduction = {

                //signin page

                titleWelcom: "EasyWinz में आपका स्वागत है",
                placeholderEmail: "ईमेल",
                placeholderPhone: "मोबाइल",
                placeholderPassword: "कुंजिका",
                rememberPassword: "पासवर्ड याद रखें",
                forgetPassword: "पासवर्ड भूल गए?",
                buttonLogin: "लॉग इन करें", 
                dontHaveAccount: "खाता नहीं है?",
                registerNow: "अभी पंजीकरण करें",

                //signup page

                placeholderName: "नाम",
                placeholderRetypePassword: "पासवर्ड फिर से लिखें",
                placeholderCountry: "देश",
                buttonRegister: "रजिस्टर करें",
                alreadyHaveAnAccount: "क्या आपके पास पहले से एक खाता मौजूद है?",
                loginNow: "अब प्रवेश करें",
                placeholderAddress: "पता",
                buttonSend: 'संदेश',
                mal: "नर",
                famel: "महिला",

                //forget password

                titleReset: "पासवर्ड रीसेट",
                buttonReset: "अनुरोध कोड रीसेट करें",
                DidntReceived: "प्राप्त नहीं हुआ",
                sendAgain: "फिर से भेजो",

                //ver code

                textVer: " सत्यापन कोड दर्ज करें",
                buttonVer: 'संदेश',

                //profile page
                profile: "प्रोफ़ाइल",
                personalDetails: "व्यक्तिगत विवरण",
                hello: "हैलो",
                activeT: "सक्रिय टिकट",
                logout: "लॉग आउट",
                setting: "स्थापना",
                language: "भाषा: हिन्दी",
                currency: "मुद्रा",
                general: "सामान्य",
                howItWorks: "यह काम किस प्रकार करता है",
                products: "उत्पाद",
                nextPrize: "अगला पुरस्कार सुझाव",
                payment: "पत्ते",
                cards: 'पत्ते',
                privacyPolicy: "गोपनीयता नीति",
                buttonCall: "हमें कॉल करें",
                buttonEmail: "हमे ईमेल करे",
                buttonUpdate: "अपडेट करें",
                alert: "क्या आप लॉग आउट करना चाहते हैं?",

                //cards

                placeholderCardNumber: "क्रेडिट या डेबिट कार्ड संख्या",
                placeholderCardName: "कार्डधारक का नाम",
                placeholderExpiry: "समाप्ति तिथि",
                placeholderCvv: "cvv सुरक्षा कोड",
                placeholderSelectType: "प्रकार चुनें",
                saveThisCard: "भविष्य की खरीदारी के लिए इस कार्ड को सहेजें",
                buttonSendCard: "संदेश",
                messageLoading: "कृपया प्रतीक्षा करें...",

                //Tickets

                tickets: "टिकट",
                ticketNumber: "टिकट नंबर :",

                //products

                productsP: "उत्पाद",

                //prize

                prize: "अगला पुरस्कार सुझाव",

                //how_it_work

                how_it_work: "यह काम किस प्रकार करता है",

                //privacyPolicy

                //  privacyPolicy : "गोपनीयता नीति"

                //product details

                productDetails: "उत्पाद विवरण",

                prizeDetails: "पुरस्कार का विवरण",
                productdetails: "उत्पाद विवरण",
                for: "for",

                //payment

                paymentPage: 'भुगतान',
                payNow: "अब भुगतान करें",
                addnewcard: "नया कार्ड जोड़ें",
                creditOrDebit: "क्रेडिट या डेबिट कार्ड",
                month: "महीना",
                year: "साल",



                //product details
                addToCart: "कार्ट में डालें",


                //home

                titleHome: 'EasyWinz',
                closing: "अभियान का समापन जल्द ही",
                getachance: "को मौका मिलता है",
                win: "जीत",
                soldOf: "की बिक्री की",
                CAvailable: "अभियान उपलब्ध हैं",
                buy: 'खरीदें',
                //cart
                inclusive: 'जिसमें 5% वैट भी शामिल है',
                grandTotal: "कुल योग",
                ProceedToPayment: 'भुगतान करने के लिए आगे बढ़ें',
                titleCart: 'गाड़ी',
                empty1: "आपकी गाड़ी खाली है",
                empty2: "इसे खुश करने के लिए कुछ आइटम जोड़ें",
                textCard: 'हर खरीद के साथ आप जीतने का मौका दोगुना करेंगे',

                //winners

                winners: "विजेताओं",
                soldOut: "बिक गया",
                congratulations: "बधाई हो",
                ticketNo: "टिकट नं:",
                announced: "की घोषणा की:",

                //wishlist

                wishlist: "इच्छा-सूची",

            }
        }
        if (localStorage.getItem('language') == 'arabic') {
            this.traduction = {

                //signin page

                titleWelcom: "EasyWinz مرحبا في",
                placeholderEmail: "البريد الإلكتروني",
                placeholderPhone: "الهاتف",
                placeholderPassword: "كلمة المرور",
                rememberPassword: "حفظ كلمة المرور",
                forgetPassword: "نسيت كلمة المرور؟",
                buttonLogin: "تسجيل الدخول",
                dontHaveAccount: "ليس لديك حساب؟",
                registerNow: "تسجيل حساب",

                //signup page

                placeholderName: "الإسم",
                placeholderRetypePassword: "إعادة كلمة المرور",
                placeholderCountry: "الدولة",
                buttonRegister: "تسجيل",
                alreadyHaveAnAccount: "عندك حساب مسبقا؟",
                loginNow: "تسجيل الدخول",
                placeholderAddress: "العنوان",
                buttonSend: 'إرسال',
                mal: "ذكر",
                famel: "أنثى",

                //forget password

                titleReset: "إعادة كلمة المرور",
                buttonReset: "طلب رمز إعادة التعيين",
                DidntReceived: "لم تتوصل بالرمز",
                sendAgain: "إعادة الإرسال",

                //ver code

                textVer: "أدخل رمز التأكد",
                buttonVer: 'إرسال',
                activate:"تفعيل", 

                //profile page
                profile: "حسابى",
                personalDetails: "التفاصيل الشخصية",
                hello: "مرحبا",
                activeT: "التذاكر النشطة",
                logout: "تسجيل الخروج",
                setting: "الإعدادات",
                language: "اللغة",
                currency: "العملة",
                general: "عام",
                howItWorks: "كيفية الإستخدام",
                products: "المنتجات",
                nextPrize: "الجوائز القادمة المقترحة",
                payment: "البطاقات",
                cards: 'البطاقات',
                privacyPolicy: "السياسة الخاصة",
                buttonCall: "اتصل بنا",
                buttonEmail: "راسلنا",
                buttonUpdate: "تعديل",
                alert: "هل تريد تسجيل الخروج",
                loginAlert: "تسجيل الدخول",
                signupAlert: "إنشاء حساب",
                alertMassage: "من فضلك أنشئ حساب أو سجل الدخول",
                //cards

                placeholderCardNumber: "رقم بطاقة الائتمان",
                placeholderCardName: "اسم البطاقة",
                placeholderExpiry: "تاريخ الإنتهاء",
                placeholderCvv: "رقم الأمان",
                placeholderSelectType: "اختر النوع",
                saveThisCard: "حفظ البطاقة",
                buttonSendCard: "إرسال",
                messageLoading: "المرجوا الإنتظار...",

                //Tickets

                tickets: "التذاكر",
                ticketNumber: "رقم التذكرة:",

                //products

                productsP: "المنتجات",

                //prize

                prize: "الجوائز القادمة المقترحة",

                //how_it_work

                how_it_work: "كيف يعمل",

                //privacyPolicy


                //product details

                productDetails: "تفاصيل المنتج",

                prizeDetails: "تفاصيل الجائزة",
                productdetails: "تفاصيل المنتج",
                for: "من أجل",

                //payment

                paymentPage: 'الدفع',
                payNow: "دفع الان",
                addnewcard: "أضف بطاقة جديدة",
                creditOrDebit: "رقم بطاقة الائتمان",
                month: "الشهر",
                year: "السنة",



                //product details
                addToCart: "إضافة للسلة",


                //home
                home : "الرئيسية",
                titleHome: 'EasyWinz',
                closing: "سينتهي قريبا",
                getachance: "احصل على فرصة",
                win: "لربح",
                soldOf: "بيعت من",
                CAvailable: "الحملات المتاحة",
                buy: 'شراء',
                //cart
                inclusive: 'شامل 5٪ ضريبة القيمة المضافة',
                grandTotal: "المجموع العام",
                ProceedToPayment: 'الشروع في دفع',
                titleCart: 'السلة',
                empty1: "السلة فارغة",
                empty2: "أضف بعض العناصر لإثارة البهجة",
                textCard: 'مع كل شرائك ستضاعف فرصتك للفوز',
                deletFromCart: "تمت إزالة المنتج",

                //winners

                winners: "الفائزين",
                soldOut: "بيعت كلها",
                congratulations: "تهانينا",
                ticketNo: "تذكرة رقم:",
                announced: "أعلن في:",

                //wishlist

                wishlist: "المفضلة",

            }
        }
    }


  
    getTraduction(){
        return this.traduction;
    }

}
