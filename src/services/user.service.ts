import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HomeService } from './home.service';
@Injectable({
    providedIn: 'root'
})
export class UserService {
    token: any = "";
    stripe_key = "pk_live_51Iw32iJvCiXIgZlvNtwWnAwefIJpnMvlah6k8IIDnYz9nfJ4dTb3bZhapOkLjwYzFkbXLdQBerUjyLE3iOh50Nab009nXr4w6c";
    strip_sk = "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"
    constructor(private http: HttpClient,
        private router: Router,
        public home: HomeService,
        private alertController: AlertController) {
        this.token = "heb3Z2rM5RAiBO2KvtEFh3xXbUaH5SXEKTvh4lWm";
        // if(!localStorage.getItem('user')){
        //     this.router.navigate(['/signin']);
        // }
    }

    async CreateStripeUser(email, user_id) {
        // await this.presentLoaingMsg("Creating Customer ...")
        const customer = {
            description: 'Easy Winz Customer',
            email: email
        };
        this.home.externalPost('https://api.stripe.com/v1/customers', customer, this.strip_sk).subscribe((customer: any) => {
            console.log(customer);
            // this.util.hide();
            if (customer && customer.id) {
                // this.cid = customer.id;
                const cid = {
                    id: localStorage.getItem('uid'),
                    stripe_key: customer.id
                };
                // let data = {
                //     user_id: user_id,
                //     stripe_key: customer.id,
                //     tokenvalue: this.token
                // }
                let data = JSON.parse(localStorage.getItem("user"));
                data.stripe_key = customer.id;
                this.updateUserDetails(data).subscribe((res: any) => {
                    console.log(res);
                    // let user = JSON.parse(localStorage.getItem("user"))
                    // data.stripe_key = customer.id;
                    localStorage.setItem('user', JSON.stringify(data));

                    console.log(res);
                })
                // this.makePayment(data.id,cid.stripe_key)
            }
            //   this.loadingCtrl.dismiss();
        }, error => {
            //   this.loadingCtrl.dismiss();
            console.log(error);

        });
    }

    updateUserStripeKey(data) {
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/updateStripeKey.php", data);
    }
    updateUserDetails(data) {
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/UpdateUserData.php", data);
    }

    updatePhoto(img) {
        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id
        }
        data.tokenvalue = this.token;
        data.user_photo = img;
        return this.http.post("https://easywinz.com/admin/mobapi/User/ChangeUserPhoto.php", data);
    }

    addCreditCard(data) {
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/addCredit.php", data);
    }

    getCreditCards() {
        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id
        }
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/getCreditCard.php ", data);
    }

    getSetting() {
        let data: any = {};
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/getAppSetting.php", data);
    }
    getTickets() {
        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id
        }
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/GetTickets.php", data);
    }
    getProducts() {
        let data: any = {}
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/getAppFixedProduct.php", data);
    }
    getNextPrize() {
        let data: any = {}
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/getNextPrize.php", data);
    }
    getWinners() {
        let data: any = {}
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/GetWinners.php", data);
    }
    getFavorites() {
        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id
        }
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/GetFavorites.php", data);
    }
    getSoldeOut() {
        let data: any = {}
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/GetSoldOut.php", data);
    }

}
