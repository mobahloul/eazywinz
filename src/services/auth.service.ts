import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OneSignal } from '@ionic-native/onesignal/ngx';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: any = "";
  oneSignalID = "bce8ec3e-8e7d-4e72-bff2-73d2850bf5f4";
  constructor(private http: HttpClient,
    private oneSignal: OneSignal
  ) {
    this.token = "heb3Z2rM5RAiBO2KvtEFh3xXbUaH5SXEKTvh4lWm";
  }

  Notifications() {
    this.oneSignal.startInit(this.oneSignalID);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

    this.oneSignal.handleNotificationReceived().subscribe(() => {
      // do something when notification is received
    });

    this.oneSignal.handleNotificationOpened().subscribe(() => {
      // do something when a notification is opened
    });

    this.oneSignal.endInit();

  }

  register(data) {
    data.tokenvalue = this.token;
    return this.http.post("https://easywinz.com/admin/mobapi/RegisterLogin/InsertUsers.php", data);
  }
  login(data) {
    data.tokenvalue = this.token;
    data.phone = data.email;
    return this.http.post("https://easywinz.com/admin/mobapi/RegisterLogin/UserLogin.php", data);
  }
  checkCode(data) {
    data.tokenvalue = this.token;
    return this.http.post("https://easywinz.com/admin/mobapi/RegisterLogin/CheckVerification.php", data);
  }

  resendCode(data) {
    data.tokenvalue = this.token;
    return this.http.post("https://easywinz.com/admin/mobapi/RegisterLogin/NewVerificationCode.php", data);
  }


  getCountry() {
    let data = {
      tokenvalue: this.token
    }
    return this.http.post("https://easywinz.com/admin/mobapi/RegisterLogin/AllCountries.php", data);
  }
  getAllCountry() {
    let data = {
      tokenvalue: this.token
    }
    return this.http.post("https://easywinz.com/admin/mobapi/RegisterLogin/AllCurrency.php", data);
  }




}
