import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
    providedIn: 'root'
})
export class HomeService {
    token: any = "";
    constructor(private http: HttpClient, private router: Router) {
        this.token = "heb3Z2rM5RAiBO2KvtEFh3xXbUaH5SXEKTvh4lWm";
        // if(!localStorage.getItem('user')){
        //     this.router.navigate(['/signin']);
        // }

    }


    httpGet(url, key) {
        const header = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', `Bearer ${key}`)
        };

        return this.http.get(url, header);
    }

    JSON_to_URLEncoded(element, key?, list?) {
        let new_list = list || [];
        if (typeof element === 'object') {
            for (let idx in element) {
                this.JSON_to_URLEncoded(
                    element[idx],
                    key ? key + '[' + idx + ']' : idx,
                    new_list
                );
            }
        } else {
            new_list.push(key + '=' + encodeURIComponent(element));
        }
        return new_list.join('&');
    }
    externalPost(url, body, key) {
        const header = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', `Bearer ${key}`)
        };
        const order = this.JSON_to_URLEncoded(body);
        console.log(order)
        return this.http.post(url, order, header);
    }



    getSlide() {
        let data: any = {};
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/GetGallary.php", data);
    }
    getAllProducts() {
        if (!localStorage.getItem('user')) {
            console.log('test')
            let data: any = {
                user_id: 0
            };
            data.tokenvalue = this.token;
            return this.http.post("https://easywinz.com/admin/mobapi/User/GetProductData.php", data);
        } else {
            let data: any = {
                user_id: JSON.parse(localStorage.getItem('user')).user_id
            };
            data.tokenvalue = this.token;
            return this.http.post("https://easywinz.com/admin/mobapi/User/GetProductData.php", data);
        }
    }

    getProduct(productId) {
        //if the user is logged sed user id else send 0
        let data: any = {
            user_id: !!localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).user_id : 0,
            product_id: productId
        };
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/GetProductDataWithProductId.php", data);
    }
    getProductsWillEndSoon() {
        let data: any = {};
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/GetProductWillEndSoon.php", data);
    }

    addLike(productId) {
        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id,
            product_id: productId
        };
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/AddLike.php", data);
    }
    deleteLike(productId) {
        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id,
            product_id: productId
        };
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/DeleteLike.php", data);
    }

}