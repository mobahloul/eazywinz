import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class CartService {
    token: any = "";
    public productsNbr: BehaviorSubject<any> = new BehaviorSubject(0);
    constructor(private http: HttpClient, private router: Router) {
        this.token = "heb3Z2rM5RAiBO2KvtEFh3xXbUaH5SXEKTvh4lWm";
        // if(!localStorage.getItem('user')){
        //     this.router.navigate(['/signin']);
        // }
    }
    addToCart(productId) {
        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id,
            product_id: productId,
            cart_amount: 1
        };
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/AddCart.php", data);
    }
    updateCart(product, action) {

        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id,
            product_id: product.product_id,
            cart_amount: product.cart_amount
        };
        data.tokenvalue = this.token;

        return this.http.post("https://easywinz.com/admin/mobapi/User/UpdateCart.php", data);
    }
    deleteFromCart(productId) {
        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id,
            product_id: productId
        };
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/DeleteCart.php", data);
    }

    getCart() {
        let data: any = {
            user_id: JSON.parse(localStorage.getItem('user')).user_id
        };
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/GetCart.php", data);
    }

    buyProduct(data) {
        data.user_id = JSON.parse(localStorage.getItem('user')).user_id
        data.tokenvalue = this.token;
        return this.http.post("https://easywinz.com/admin/mobapi/User/BuyProduct.php", data);
    }
}