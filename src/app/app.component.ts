import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UserService } from 'src/services/user.service';
import { AuthService } from 'src/services/auth.service';
import { CartService } from 'src/services/cart.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  /////
  language: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authSevice: AuthService,
    private cartService: CartService,
  ) {
    if (localStorage.getItem('language')) {
      this.language = localStorage.getItem('language');
      if (this.language == 'english') {
        this.read_data('./assets/traduction-english.json');
      }
      if (this.language == 'arabic') {
        this.read_data('./assets/traduction-arabic.json');
      }
      if (this.language == 'indian') {
        this.read_data('./assets/traduction-indian.json');
      }
    } else {
      localStorage.setItem('language', 'english');
      this.read_data('./assets/traduction-english.json');
    }

    if (!localStorage.getItem('currency')) {
      localStorage.setItem('currency', 'AED');
    }
    if (!localStorage.getItem('currency_price_for_AED')) {
      this.authSevice.getAllCountry().subscribe(
        (data: any) => {
          console.log(data)
          localStorage.setItem('currency_price_for_AED', data.Country[0].currency_price_for_AED);
        }
      )
    }
 
    this.initializeApp();

  }

  read_data(traduction) {
    fetch(traduction).then(res => res.json())
      .then(json => {
        console.log("hererer")
        localStorage.setItem('traduction', JSON.stringify(json));
      });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (localStorage.getItem('user')) {
        this.authSevice.Notifications();
        this.cartService.getCart().subscribe(
          (data: any) => {
            if (data.Favorites.length > 0) {
              let nbr = 0;
              data.Favorites.forEach((element, index) => {
                nbr = nbr + Number(element.cart_amount);
                if (index == data.Favorites.length - 1) {
                  this.cartService.productsNbr.next(nbr);
                }
              });
            }
          }
        )
      }
      setTimeout(function () {
        document.getElementById("splash-overlay").style.display = "none";
      }, 3500);
    });
  }
}
