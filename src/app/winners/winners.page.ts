import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/services/user.service';
import { TraductionService } from 'src/services/traduction.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-winners',
  templateUrl: './winners.page.html',
  styleUrls: ['./winners.page.scss'],
})
export class WinnersPage {
  tabs: any = 0;
  winners: any = [];
  soldOut: any = [];
  traduction: any = {};
  language: any;
  constructor(private userService: UserService, private router: Router) {
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.language = localStorage.getItem('language');
  }

  OnInit() { }
  ionViewWillEnter() {
    // if (!localStorage.getItem('user')) {
    //   this.router.navigate(['/signin']);
    // }else{
    this.userService.getWinners().subscribe(
      (data: any) => {
        this.winners = data.Winners;
        console.log(data)
      }
    )
    this.userService.getSoldeOut().subscribe(
      (data: any) => {
        this.soldOut = data.Products;
        console.log(data)
      }
    )
    //}
  }

}
