import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.page.html',
  styleUrls: ['./privacy-policy.page.scss'],
})

export class PrivacyPolicyPage implements OnInit {
  traduction: any = {};
  setting: any = [];
  language: any;
  pdfSrc: any;
  check: number;
  constructor(private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.language = localStorage.getItem('language');
    this.route.queryParams.subscribe(params => {
      console.log(params.check)
      if (params.check) {
        this.check = params.check
        if (params.check == 0) {

          if (this.language == 'english') {
            this.pdfSrc = 'https://www.easywinz.com/documents/privacy_policy_en.pdf';
          }
          if (this.language == 'arabic') {
            this.pdfSrc = 'https://www.easywinz.com/documents/privacy_policy_ar.pdf';
          }
          if (this.language == 'indian') {
            this.pdfSrc = 'https://www.easywinz.com/documents/privacy_policy_en.pdf';
          }
        }
        if (params.check == 1) {
          if (this.language == 'english') {
            this.pdfSrc = 'https://www.easywinz.com/documents/term_en.pdf';
          }
          if (this.language == 'arabic') {
            this.pdfSrc = 'https://www.easywinz.com/documents/term_ar.pdf';
          }
          if (this.language == 'indian') {
            this.pdfSrc = 'https://www.easywinz.com/documents/term_en.pdf';
          }
        }
        if (params.check == 2) {
          if (this.language == 'english') {
            this.pdfSrc = 'https://www.easywinz.com/documents/user_agrement_en.pdf';
          }
          if (this.language == 'arabic') {
            this.pdfSrc = 'https://www.easywinz.com/documents/user_argement_ar.pdf';
          }
          if (this.language == 'indian') {
            this.pdfSrc = 'https://www.easywinz.com/documents/user_argement_en.pdf';
          }
        }
      }
    })
    this.traduction = JSON.parse(localStorage.getItem('traduction'));

  }

ngOnInit() {
  this.getSetting();
}
getSetting() {
  this.userService.getSetting().subscribe(
    (data: any) => {
      this.setting = data.App_Setting[0];
    }
  )
}
back() {
  this.router.navigate(['/profile'])
}

}
