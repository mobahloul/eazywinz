import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RPolicyPageRoutingModule } from './rpolicy-routing.module';

import { RPolicyPage } from './rpolicy.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RPolicyPageRoutingModule
  ],
  declarations: [RPolicyPage]
})
export class RPolicyPageModule {}
