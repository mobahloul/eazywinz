import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RPolicyPage } from './rpolicy.page';

const routes: Routes = [
  {
    path: '',
    component: RPolicyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RPolicyPageRoutingModule {}
