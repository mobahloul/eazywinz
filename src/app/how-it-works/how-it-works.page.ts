import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/services/user.service';
import { Router } from '@angular/router';
import { TraductionService } from 'src/services/traduction.service';
import { VideoPlayer } from '@ionic-native/video-player/ngx';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.page.html',
  styleUrls: ['./how-it-works.page.scss'],
  providers : [VideoPlayer]
})
export class HowItWorksPage implements OnInit {
  setting: any = {};
  traduction: any = {};
  language: any;
  constructor(private userService: UserService,
    private router: Router,
    private videoPlayer: VideoPlayer,
    private traductionService: TraductionService) {
      this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.language = localStorage.getItem('language');
  }

  ngOnInit() {
    this.getSetting();
  }
  playV(url) {
    // Playing a video.
    this.videoPlayer.play(url).then(() => {
      console.log('video completed');
    }).catch(err => {
      console.log(err);
    });
  }
  getSetting() {
    this.userService.getSetting().subscribe(
      (data: any) => {
        this.setting = data.App_Setting[0];
        this.playV(this.setting.youtube_link)
      }
    )

  }
  back() {
    this.router.navigate(['/profile'])
  }
}
