import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/services/home.service';
import { UserService } from 'src/services/user.service';
import { Router } from '@angular/router';
import { AlertsService } from 'src/services/alerts.service';
import { CartService } from 'src/services/cart.service';
import { TraductionService } from 'src/services/traduction.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  providers: [HomeService]
})
export class HomePage {
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    autoplay: true,
    loop: true
  };

  slide: any = [];
  products: any = [];
  productsWillEndSoon: any = [];
  currency: any;
  rate: number;
  loader = false;
  traduction: any = {};
  loading: any;
  language: any;
  constructor(private homeService: HomeService,
    private router: Router,
    private traductionService: TraductionService,
    private alertService: AlertsService,
    private cartService: CartService,
    public loadingCtrl: LoadingController,
    private userService: UserService) {

    this.language = localStorage.getItem('language');

  }

  OnInit() { }
  ionViewWillEnter() {
    this.currency = localStorage.getItem('currency');
    this.rate = Number(localStorage.getItem('currency_price_for_AED'));
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.homeService.getSlide().subscribe(
      (data: any) => {
        this.slide = data.Products;
      }

    )
    this.homeService.getProductsWillEndSoon().subscribe(
      (data: any) => {
        this.productsWillEndSoon = data.Products;
      }
    )
    this.homeService.getAllProducts().subscribe(
      (data: any) => {
        this.products = data.Products;
      }
    )
    if (!localStorage.getItem('user')) return;
    this.cartService.getCart().subscribe(
      (data: any) => {
        if (data.Favorites.length > 0) {
          let nbr = 0;
          data.Favorites.forEach((element, index) => {
            nbr = nbr + Number(element.cart_amount);
            if (index == data.Favorites.length - 1) {
              this.cartService.productsNbr.next(nbr);
            }
          });
        }

      }
    )
  }
  goToProductDetails(id, k) {
    // if (!localStorage.getItem('user')) {
    //   this.router.navigate(['/signin']);
    // } else {
    if (k == 0) {
      this.router.navigate(['tabs/product-details', id, 0]);
    } else {
      this.router.navigate(['tabs/product-details', id, 1]);
    }
    //}
  }
  addToCart(product_id) {
    if (!localStorage.getItem('user')) {
      this.router.navigate(['/signin']);
    } else {
      this.presentLoading();
      this.cartService.addToCart(product_id).subscribe(
        (data: any) => {
          this.hideLoader();
          if (data) {
            this.alertService.presentToast('product added successfully');
            this.cartService.getCart().subscribe(
              (data: any) => {
                if (data.Favorites.length > 0) {
                  let nbr = 0;
                  data.Favorites.forEach((element, index) => {
                    nbr = nbr + Number(element.cart_amount);
                    if (index == data.Favorites.length - 1) {
                      this.cartService.productsNbr.next(nbr);
                    }
                  });
                }

              }
            )
            // this.cartService.productsNbr.subscribe(
            //   (data) => {
            //     console.log(data)
            //     this.cartService.productsNbr.next(data + 1);
            //   }
            // )
          } else {
            this.alertService.presentToast('error');
          }
        }, (err) => {
          this.alertService.presentToast(err);
          this.hideLoader();
        }
      )
    }
  }
  presentLoading() {
    this.loadingCtrl.create({
      message: this.traduction.messageLoading
    }).then((res) => {
      res.present();
    });
  }
  hideLoader() {
    this.loadingCtrl.dismiss().then((res) => {
    }).catch((error) => {
    });
  }
  addLike(i) {
    if (!localStorage.getItem('user')) {
      this.router.navigate(['/signin']);
    } else {
      this.homeService.addLike(this.products[i].product_id).subscribe(
        (data: any) => {
          if (data) {
            this.products[i].productlike = 1;
          }
          console.log(data)
        }
      )
    }
  }
  deleteLike(i) {
    if (!localStorage.getItem('user')) {
      this.router.navigate(['/signin']);
    } else {
      this.homeService.deleteLike(this.products[i].product_id).subscribe(
        (data: any) => {
          if (data) {
            this.products[i].productlike = 0;
          }
          console.log(data)
        }
      )
    }
  }

}
