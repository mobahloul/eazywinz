import { Component } from '@angular/core';
import { TraductionService } from 'src/services/traduction.service';
import { CartService } from 'src/services/cart.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  traduction : any = {};
  pNbr : number = 0;
  constructor(private cartService : CartService) {
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.cartService.productsNbr.subscribe(
      (data: any) => {
        this.pNbr = data;
      }
    )
  }

}
