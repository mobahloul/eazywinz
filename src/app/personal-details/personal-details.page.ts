import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/services/user.service';
import { Router } from '@angular/router';
import { AlertsService } from 'src/services/alerts.service';
import { AuthService } from 'src/services/auth.service';
import { TraductionService } from 'src/services/traduction.service';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.page.html',
  styleUrls: ['./personal-details.page.scss'],
  providers: [UserService]
})
export class PersonalDetailsPage implements OnInit {
  user: any = {};
  countrys: any = [];
  loader: boolean = false;
  traduction: any = {};
  language: any;
  countryTmp: string = "";
  opened: boolean = false;
  constructor(private userSevice: UserService, private authSevice: AuthService, private router: Router,
    private alertsService: AlertsService, private traductionService: TraductionService) {
    this.language = localStorage.getItem('language');
    this.user = {
      "frist_name": "",
      "last_name": "",
      "email": "",
      "phone": "",
      //"nationality": "",
      "country": "",
      "user_type": 2,
    }
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.user = JSON.parse(localStorage.getItem('user'));
    
  }

  ngOnInit() {
   
  }
  ionViewWillEnter(){
    this.authSevice.getAllCountry().subscribe(
      (data: any) => {
        this.countrys = data.Country;
        console.log(this.countrys);
        this.countryTmp = this.countrys.filter(val => val.country_id === this.user.country)[0].country_name
      }
    )
  }
  setOpened(){
    console.log('i am here')
    this.opened = true
  }
  back() {
    this.router.navigate(['/profile'])
  }
  validate(phone) {
    console.log(phone)
    var regex = /^\+(?:[0-9] ?){6,11}[0-9]$/;
    console.log(regex.test(phone))
    if (regex.test(phone)) {
     
      // Valid international phone number
      return true;
    } else {
      // Invalid international phone number
      return false;
    }
  }
  update() {
    this.user.user_id = localStorage.getItem('user_id');
    this.loader = true;
    // this.user.last_name = this.user.first_name;
    console.log(this.user)
    if (this.user.frist_name.trim() == '' || this.user.last_name.trim() == ''
      || this.user.email.trim() == ''
      || this.user.phone.trim() == '') {
      this.alertsService.presentToast(this.traduction.allReqFields);
      this.loader = false;
    } else {
      if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.user.email))) {
        this.alertsService.presentToast(this.traduction.validEmail);
        this.loader = false;
      } else {
       
        if (!this.validate(this.user.phone)) {
          this.alertsService.presentToast(this.traduction.validPhone);
          this.loader = false;
        }

        else {

          this.userSevice.updateUserDetails(this.user).subscribe(
            (data: any) => {
              console.log(data);
              localStorage.setItem('user',JSON.stringify(this.user));
              localStorage.setItem('phone', this.user.phone);
              this.router.navigate(['/profile']);
            }, (err: any) => {
              this.loader = false;
            }
          )
        }
      }
    }
  }
}
