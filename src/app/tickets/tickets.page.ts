import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/services/user.service';
import { TraductionService } from 'src/services/traduction.service';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.page.html',
  styleUrls: ['./tickets.page.scss'],
})
export class TicketsPage implements OnInit {
  tickets : any = [];
  traduction : any = {};
  language : any;
  constructor(private userService : UserService, private traductionService : TraductionService) { 
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.language = localStorage.getItem('language');
  }

  ngOnInit() {
    this.userService.getTickets().subscribe(
      (data:any) =>{
        this.tickets = data.Tickets;
      }
    )
  }
  

}
