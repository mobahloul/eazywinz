import { Component } from '@angular/core';
import { CartService } from 'src/services/cart.service';
import { UserService } from 'src/services/user.service';
import { LoadingController } from '@ionic/angular';
import { TraductionService } from 'src/services/traduction.service';
import { Router } from '@angular/router';
import { AlertsService } from 'src/services/alerts.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage {
  products: any = [];
  currency: any = "";
  rate: number;
  total: number = 0;
  traduction: any = {};
  loading: any;
  language: any;
  pNbr: number = 0;


  constructor(private cartService: CartService,
    private loadingCtrl: LoadingController,
    private traductionService: TraductionService,
    private router: Router,
    private alertService: AlertsService,
    private alertController: AlertController,
    private userService: UserService,
  ) {
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    console.log(this.traduction)
    this.language = localStorage.getItem('language');
  }

  OnInit() {
  }
  ionViewWillEnter() {
    if (!localStorage.getItem('user')) {
      this.router.navigate(['/signin']);
    } else {
      this.getCart();
    }
  }
  getCart() {
    this.total = 0;
    this.presentLoading();
    this.currency = localStorage.getItem('currency');
    //this.traduction = this.traductionService.traduction;
    this.rate = Number(localStorage.getItem('currency_price_for_AED'));

    console.log()
    this.cartService.getCart().subscribe(
      (data: any) => {
        this.products = data.Favorites;
        console.log(this.products)
        if (this.products.length > 0) {
          this.products.forEach((element, index) => {
            this.total = this.total + (Number(element.product_prise) * Number(element.cart_amount));
            this.hideLoader();
            if (index == this.products.length - 1) {
              this.hideLoader();
              this.total = this.total * Number(localStorage.getItem('currency_price_for_AED'));
            }
          });
        } else {
          this.hideLoader();
        }
        if (data.Favorites.length > 0) {
          let nbr = 0;
          data.Favorites.forEach((element, index) => {
            nbr = nbr + Number(element.cart_amount);
            if (index == data.Favorites.length - 1) {
              this.cartService.productsNbr.next(nbr);
            }
          });
        }
        else {
          this.cartService.productsNbr.next(0)
        }

      }
    )
  }
  dec(i) {
    if (this.products[i].cart_amount > 1) {
      this.products[i].cart_amount--;
      this.cartService.updateCart(this.products[i], "dec").subscribe(
        (data: any) => {
          //this.myData.next("recount");
          this.getCart()

        }
        , (err) => {

        }

      )
    }
    else {
      this.presentAlertConfirm(i)
    }
  }
  inc(i) {
    this.products[i].cart_amount++;
    this.cartService.updateCart(this.products[i], "inc").subscribe(
      (data: any) => {
        console.log(data)
        //this.myData.next("recount");
        this.getCart()

      }
      , (err) => {
        console.error(err)
      }

    )
  }
  async presentLoading() {
    await this.loadingCtrl.create({
      message: this.traduction.messageLoading
    }).then((res) => {
      res.present();
    });
  }
  hideLoader() {
    this.loadingCtrl.dismiss().then((res) => {
    }).catch((error) => {
    });
  }

  toPayment() {
    this.router.navigate(['payment/1']);
  }
  deleteFromCart(i) {
    console.log(" i am deleting")
    this.cartService.deleteFromCart(this.products[i].product_id).subscribe(
      (data: any) => {
        console.log(data)
        if (data.result) {
          this.alertService.presentToast(this.traduction.deletFromCart)
          this.getCart();





        } else {
          this.alertService.presentToast('Error')
        }
      }
      , (err) => {
        this.alertService.presentToast('Error')
      }

    )
  }

  async presentAlertConfirm(i) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: this.traduction.deleteItemFromCart,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.deleteFromCart(i)
          }
        }
      ]
    });

    await alert.present();
  }
}
