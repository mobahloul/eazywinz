import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/services/user.service';
import { CartService } from 'src/services/cart.service';
import { AlertsService } from 'src/services/alerts.service';
import { TraductionService } from 'src/services/traduction.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HomeService } from 'src/services/home.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.page.html',
  styleUrls: ['./wishlist.page.scss'],
})
export class WishlistPage {
  products: any = [];
  currency: any = "";
  rate: number;
  loader : any;
  traduction: any = {};
  language : any;
  constructor(private userService: UserService,
    private traductionService : TraductionService, 
    private alertService: AlertsService, 
    private loadingCtrl : LoadingController,
    private homeService: HomeService,
    private router : Router,
    private cartService: CartService) {
    this.currency = localStorage.getItem('currency');
    this.rate = Number(localStorage.getItem('currency_price_for_AED'));
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.language = localStorage.getItem('language');
  }
  OnInit(){}
  ionViewWillEnter() {
    if (!localStorage.getItem('user')) {
      this.router.navigate(['/signin']);
    }else{
      this.userService.getFavorites().subscribe(
        (data: any) => {
          this.products = data.Favorites;
        }
      )
    }
  }
  addToCart(product_id) {
    this.presentLoading();
    this.cartService.addToCart(product_id).subscribe(
      (data: any) => {
        this.hideLoader();
        if (data) {
          this.cartService.getCart().subscribe(
            (data: any) => {
              if (data.Favorites.length > 0) {
                let nbr = 0;
                data.Favorites.forEach((element, index) => {
                  nbr = nbr + Number(element.cart_amount);
                  if (index == data.Favorites.length - 1) {
                    this.cartService.productsNbr.next(nbr);
                  }
                });
              }
      
            }
          )
          this.alertService.presentToast('product added successfully');
        }else{
          this.alertService.presentToast('error');
        }
      }, (err) => {
        this.alertService.presentToast(err);
        this.hideLoader();
      }
    )
  }

  goToProductDetails(id, k) {
    if (k == 0) {
      this.router.navigate(['/product-details', id, 0]);
    } else {
      this.router.navigate(['/product-details', id, 1]);
    }
  }

  addLike(id) {
    this.homeService.addLike(id).subscribe(
      (data: any) => {
        if (data) {
          console.log(data)
        }
        console.log(data)
      }
    )
  }
  deleteLike(id) {
    this.presentLoading();
    this.homeService.deleteLike(id).subscribe(
      (data: any) => {
        if (data) {
          this.userService.getFavorites().subscribe(
            (data: any) => {
              this.products = data.Favorites;
              this.hideLoader()
            }
          )
        }
        console.log(data)
      }
    )
  }
  
  presentLoading() {
    this.loadingCtrl.create({
      message: this.traduction.messageLoading
    }).then((res) => {
      res.present();
    });
  }
  hideLoader() {
    this.loadingCtrl.dismiss().then((res) => {
    }).catch((error) => {
    });
  }


}
