import { Component, OnInit } from '@angular/core';
import { TraductionService } from 'src/services/traduction.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {

  traduction: any = {};
  constructor(private traductinService: TraductionService) {
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
  }

  ngOnInit() {
  }

}
