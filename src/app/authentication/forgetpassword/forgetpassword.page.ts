import { Component, OnInit } from '@angular/core';
import { TraductionService } from 'src/services/traduction.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.page.html',
  styleUrls: ['./forgetpassword.page.scss'],
})
export class ForgetpasswordPage implements OnInit {
  traduction: any = {};
  constructor(private traductinService: TraductionService) {
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
  }

  ngOnInit() {
  }

}
