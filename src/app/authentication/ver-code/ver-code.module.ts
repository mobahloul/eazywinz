import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerCodePageRoutingModule } from './ver-code-routing.module';

import { VerCodePage } from './ver-code.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerCodePageRoutingModule
  ],
  declarations: [VerCodePage]
})
export class VerCodePageModule {}
