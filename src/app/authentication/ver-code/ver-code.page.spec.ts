import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VerCodePage } from './ver-code.page';

describe('VerCodePage', () => {
  let component: VerCodePage;
  let fixture: ComponentFixture<VerCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerCodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VerCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
