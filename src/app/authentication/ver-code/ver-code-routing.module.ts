import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerCodePage } from './ver-code.page';

const routes: Routes = [
  {
    path: '',
    component: VerCodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VerCodePageRoutingModule {}
