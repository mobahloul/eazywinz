import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
import { Router } from '@angular/router';
import { TraductionService } from 'src/services/traduction.service';
import { AlertsService } from 'src/services/alerts.service';

@Component({
  selector: 'app-ver-code',
  templateUrl: './ver-code.page.html',
  styleUrls: ['./ver-code.page.scss'],
})
export class VerCodePage implements OnInit {
  user: any;
  verification_code: number;
  traduction: any = {};
  constructor(private authService: AuthService,
    private traductionService: TraductionService,
    private alertsService: AlertsService,
    private router: Router) {
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user)
    if (this.user.verify_status === "0") {
      let data = {
        email: this.user.email,
        password: this.user.password
      }
      this.authService.resendCode(data).subscribe(
        (res: any) => {
          console.log(res)
          this.alertsService.presentToast(this.traduction.codeSent);
        }
      )
    }
  }

  ngOnInit() {
  }

  resendVerificationCode() {
    let data = {
      email: this.user.email,
      password: this.user.password
    }
    this.authService.resendCode(data).subscribe(
      (data: any) => {
        this.alertsService.presentToast(this.traduction.codeSent);
      }
    )
  }

  send() {
    let data = {
      verification_code: this.verification_code,
      phone: localStorage.getItem('phone')
    }
    this.authService.checkCode(data).subscribe(
      (res: any) => {
        res.result;
        if (res.result == true && res.Users.length > 0) {
          localStorage.setItem('user', JSON.stringify(res.Users[0]));
          this.router.navigate(['/home']);
        } else {
          this.alertsService.presentToast(res.result);
        }
      }
    )
  }
}
