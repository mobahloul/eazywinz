import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AlertsService } from 'src/services/alerts.service';
import { AuthService } from 'src/services/auth.service';
import { TraductionService } from 'src/services/traduction.service';
import { UserService } from 'src/services/user.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
  providers: [AlertsService]
})
export class SigninPage implements OnInit {
  user: any = {};
  traduction: any = {};
  loader: boolean = false;
  constructor(private navCtrl: NavController, private traductionService: TraductionService,
    private router: Router, public userService: UserService,
    private authService: AuthService, private alertsService: AlertsService) {
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.user = {
      "email": "",
      "password": ""
    }
  }

  ngOnInit() {
    // if(localStorage.getItem('user')){
    //   this.router.navigate(['/tabs/home']);
    // }
  }
  login() {
    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.user.email))) {
      this.alertsService.presentToast(this.traduction.validEmail);
      this.loader = false;
    } else {
      if (this.user.password.trim().length < 6) {
        this.alertsService.presentToast(this.traduction.emptyPassword);
        this.loader = false;
      } else {
        this.loader = true;
        this.authService.login({ email: this.user.email, password: this.user.password, pdv: this.user.pdv }).subscribe(
          (data: any) => {
            console.log(data.Users.length)
            if (data.Users.length) {
              if (data.Users[0].verify_status !== "0") {
                localStorage.setItem('user', JSON.stringify(data.Users[0]));
                if (!data.Users[0].stripe_key || data.Users[0].stripe_key == "")
                  this.userService.CreateStripeUser(data.Users[0].email, data.Users[0].user_id)
                    ; this.authService.Notifications();
                this.router.navigate(['/tabs/home']);
                console.log(data)
              }
              else {
                localStorage.setItem('user', JSON.stringify(data.Users[0]));
                this.router.navigate(['/code']);
              }

            }
            else {
              this.alertsService.presentToast(this.traduction.emailOrPswIncorrect);
            }
            // this.router.navigate(['/']);
          }, (err: any) => {
            this.loader = false;
            this.alertsService.presentToast(this.traduction.emailOrPswIncorrect);
          }
        )
      }
    }
  }

}
