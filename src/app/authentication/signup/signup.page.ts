import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
import { AlertsService } from 'src/services/alerts.service';
import { Router } from '@angular/router';
import { TraductionService } from 'src/services/traduction.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
  providers: [AlertsService]
})
export class SignupPage implements OnInit {
  user: any = {};
  countrys: any = [];
  loader: boolean = false;
  traduction: any = {};
  language:any;
  constructor(private authSevice: AuthService, private traductionService: TraductionService, private router: Router,
    private alertsService: AlertsService, public loadingCtrl: LoadingController) {
     this.language= localStorage.getItem('language')
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.user = {
      "frist_name": "",
      "last_name": "",
      "email": "",
      "country_code": "+971",
      "phone": "",
      "gender": "",
      "password": "",
      "nationality": 1,
      "country": 1,
      "user_type": 2,
      "address": "",
      "confirmePassword": "",
      "tos": false
    }
  }
  async presentLoading() {
    await this.loadingCtrl.create({
      message: this.traduction.messageLoading
    }).then((res) => {
      res.present();
    });
  }
  hideLoader() {
    this.loadingCtrl.dismiss().then((res) => {
    }).catch((error) => {
    });
  }
  ngOnInit() {
    this.authSevice.getCountry().subscribe(
      (data: any) => {
        this.countrys = data.Country;
        console.log(data);
      }
    )
  }
  onChange(event) {
    console.log(event)
  }
  signup() {
    console.log(this.user)

    if (this.user.frist_name.trim() == '' || this.user.last_name.trim() == '' || this.user.email.trim() == ''
      || this.user.phone.trim() == '' || this.user.password.trim() == ''
      || this.user.confirmePassword.trim() == '') {
      this.alertsService.presentToast(this.traduction.allReqFields);
    } else {
      let cuntry = this.countrys.filter(item => item.country_id == this.user.country);

      if (!cuntry || (cuntry && cuntry[0].country_phone != this.user.country_code)) {
        this.alertsService.presentToast("Please select valid country");
      } else {
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.user.email))) {
          this.alertsService.presentToast(this.traduction.validEmail);
        } else {
          if (this.user.country_code == "+971") {
            if (!this.user.phone.match(/^\d{9}$/)) {
              this.alertsService.presentToast(this.traduction.validPhone);
              return;
            }
          } else {
            if (!this.user.phone.match(/^\d{10}$/)) {
              this.alertsService.presentToast(this.traduction.validPhone);
              return;
            }
          }
          if (!this.user.phone.match(/^\d{9}$/) && !this.user.phone.match(/^\d{10}$/)) {
            this.alertsService.presentToast(this.traduction.validPhone);
          } else {
            if (this.user.password.trim().length < 6) {
              this.alertsService.presentToast(this.traduction.validPSW);
            } else {
              if (this.user.password !== this.user.confirmePassword) {
                this.alertsService.presentToast(this.traduction.passwordMatch);
              } else {
                this.presentLoading();
                this.user.phone = this.user.country_code + this.user.phone;
                this.authSevice.register(this.user).subscribe(
                  (data: any) => {
                    console.log(data);
                    if (data.User && data.User.length > 0) {
                      localStorage.setItem('phone', this.user.phone);
                      localStorage.setItem("user", JSON.stringify(data.User[0]))
                      this.router.navigate(['/code']);
                    } else {
                      this.alertsService.presentToast(data.result);
                    }
                    this.hideLoader();
                  }, (err: any) => {
                    this.hideLoader();
                  }
                )
              }
            }
          }
        }
      }
    }
  }
}
