import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.page.html',
  styleUrls: ['./thanks.page.scss'],
})
export class ThanksPage implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  home() {
    this.router.navigate(['/tabs/home'], { replaceUrl: true });

  }
  tickets() {
    this.router.navigate(['/tickets'], { replaceUrl: true });
  }
}
