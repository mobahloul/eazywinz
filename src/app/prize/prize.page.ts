import { Component, OnInit } from '@angular/core';
import { TraductionService } from 'src/services/traduction.service';
import { UserService } from 'src/services/user.service';

@Component({
  selector: 'app-prize',
  templateUrl: './prize.page.html',
  styleUrls: ['./prize.page.scss'],
})
export class PrizePage implements OnInit {
  traduction : any = {};
  prizes : any = [];
  language : any;
  constructor(private  traductionService : TraductionService, private userService : UserService) { 
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.language = localStorage.getItem('language');
  }

  ngOnInit() {
    this.userService.getNextPrize().subscribe(
      (data: any) => {
        this.prizes = data.NextPrize;
      }
    )
  }

}
