import { Component, OnInit } from '@angular/core';
import { TraductionService } from 'src/services/traduction.service';
import { ActivatedRoute } from '@angular/router';
import { HomeService } from 'src/services/home.service';
import { CartService } from 'src/services/cart.service';
import { AlertsService } from 'src/services/alerts.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage {
  traduction: any = {};
  id: any;
  product: any = {};
  tabs = 1;
  perc: number = 0;
  loader: any;
  language: any;
  currency : any = "";
  rate: number;
  liked: boolean = false;
  constructor(private traductionService: TraductionService,
    private cartService: CartService,
    private alertService: AlertsService,
    private homeService: HomeService,
    private loadingCtrl: LoadingController,
    private router : Router,
    private route: ActivatedRoute) {
    this.language = localStorage.getItem('language');
    this.currency = localStorage.getItem('currency');
    this.rate = Number(localStorage.getItem('currency_price_for_AED'));
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.route.params.subscribe(params => {
      this.id = params['idProduct'];
      if (params['prize'] == 1) {
        this.tabs = 0;
      }
      this.homeService.getProduct(this.id).subscribe(
        (data: any) => {
          this.product = data.Products[0];
          this.perc = (this.product.sold_amount * 100) / this.product.total_amount;
          this.liked = this.product.productlike == 0 ? false : true
        }
      )
    });
  }


  addLike(id) {
    if (!localStorage.getItem('user')) {
      this.router.navigate(['/signin']);
    }else{
      this.homeService.addLike(id).subscribe(
        (data: any) => {
          if (data) {
            this.product.productlike = 1;
            this.liked = true
          }
          console.log(data)
        }
      )
    }
  }
  
  deleteLike(id) {
    this.homeService.deleteLike(id).subscribe(
      (data: any) => {
        if (data) {
          this.product.productlike = 0;
          this.liked = false
        }
        console.log(data)
      }
    )
  }
  addToCart(product_id) {
    this.presentLoading();
    this.cartService.addToCart(product_id).subscribe(
      (data: any) => {
        this.hideLoader();
        if (data) {
          this.alertService.presentToast('product added successfully');
        } else {
          this.alertService.presentToast('error');
        }
      }, (err) => {
        this.alertService.presentToast(err);
        this.hideLoader();
      }
    )
  }
  presentLoading() {
    this.loadingCtrl.create({
      message: this.traduction.messageLoading
    }).then((res) => {
      res.present();
    });
  }
  hideLoader() {
    this.loadingCtrl.dismiss().then((res) => {
    }).catch((error) => {
    });
  }

}
