import { Component, OnInit } from '@angular/core';
import { TraductionService } from 'src/services/traduction.service';
import { AlertController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { UserService } from 'src/services/user.service';
import { AuthService } from 'src/services/auth.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransferObject, FileTransfer } from '@ionic-native/file-transfer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from "@ionic-native/file/ngx";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CartService } from 'src/services/cart.service';
import { AlertsService } from 'src/services/alerts.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
  providers: [Camera]
})
export class ProfilePage {
  isConnected: Boolean = false;
  traduction: any = {};
  user: any = {};
  setting: any = {};
  countrys: any = [];
  currency: any = "";
  language: any = "";
  options: CameraOptions;
  fileTransfer: FileTransferObject;
  constructor(private traductionService: TraductionService,
    private router: Router,
    private camera: Camera,
    private userService: UserService,
    private authSevice: AuthService,
    private fileOpener: FileOpener,
    private transfer: FileTransfer,
    private file: File,
    private cartService: CartService,
    private alertsService: AlertsService,
    //public inAppBrowser: InAppBrowser,
    private alertController: AlertController) {
    this.options = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    }

  }

  OnInit() { }
  ionViewWillEnter() {
    this.currency = localStorage.getItem('currency');
    this.language = localStorage.getItem('language');
    console.log(this.language, this.currency)
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.authSevice.getAllCountry().subscribe(
      (data: any) => {
        this.countrys = data.Country;
        console.log(data);
      }
    )
    if (localStorage.getItem('user')) {
      this.isConnected = true;
     
      this.user = JSON.parse(localStorage.getItem('user'));
      
      this.userService.getSetting().subscribe(
        (data: any) => {
          this.setting = data.App_Setting[0];
        }
      )
      this.authSevice.getAllCountry().subscribe(
        (data: any) => {
          this.countrys = data.Country;
          console.log(data);
        }
      )
      //    this.alertLogin();
    } else {
      this.isConnected = false
      //this.router.navigate(['/signin']);
      console.log('%c⧭', 'color: #00e600', this.isConnected);
    }

  }

  async alertLogin() {
    const alert = await this.alertController.create({
      message: this.traduction.alertMassage,
      buttons: [
        {
          text: this.traduction.loginAlert,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['/signin']);
          }
        }, {
          text: this.traduction.signupAlert,
          handler: () => {
            this.router.navigate(['/signup']);
          }
        }
      ]
    });

    await alert.present();
  }
  setCurrency(event) {
    console.log(event.target.value)

    let rateTmp = this.countrys.filter(val =>val.country_currency == event.target.value)[0];
    console.log(rateTmp)
    if(rateTmp){
      localStorage.setItem('currency', rateTmp.country_currency)
      localStorage.setItem('currency_price_for_AED',rateTmp.currency_price_for_AED)
    }

  }
  setLanguage(event) {
    
    if(event.target.value !== this.language){
      localStorage.setItem('language', event.target.value);
      window.location.reload()
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: this.traduction.alert,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.router.navigate(['/tabs/home']);
            //localStorage.setclear();
            localStorage.removeItem('user')
            console.log('Confirm Okay');
            this.cartService.productsNbr.next(0);
          }
        }
      ]
    });

    await alert.present();
  }
  changePhoto() {
    this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = imageData;
      this.userService.updatePhoto(base64Image).subscribe(
        (data: any)=>{
          console.log('uploaded')
        }
      )
      this.user.user_photo = "data:image/jpeg;base64,"+imageData
      //window.location.reload()

    }, (err) => {
      // Handle error
    });
  }

  seePdf() {
    if (localStorage.getItem('language') == 'english') {
      this.download('https://www.easywinz.com/documents/privacy_policy_en.pdf', 'privacy policy')
    }
    if (localStorage.getItem('language') == 'indian') {
      this.download('https://www.easywinz.com/documents/privacy_policy_en.pdf', 'privacy policy')
    }
    if (localStorage.getItem('language') == 'arabic') {
      this.download('https://www.easywinz.com/documents/privacy_policy_ar.pdf', 'سياسة الخصوصية')
    }
  }
  download(url: string, title: string) {
    this.fileTransfer = this.transfer.create();
    this.fileTransfer
      .download(url, this.file.dataDirectory + title + ".pdf")
      .then(entry => {
        console.log("download complete: " + entry.toURL());
        this.fileOpener
          .open(entry.toURL(), "application/pdf")
          .then(() => console.log("File is opened"))
          .catch(e => console.log("Error opening file", e));
      });
  }

  //user_agrement_en.pdf
  //terms_en.pdf
  //https://www.easywinz.com/documents/easywinz_how_work_video.mp4
  goToDocs(doc: string){
        if(localStorage.getItem('language') === "english"){
          InAppBrowser.create("https://www.easywinz.com/documents/"+doc+"_en.html");
        }
        else if(localStorage.getItem('language') === "arabic"){
          InAppBrowser.create("https://www.easywinz.com/documents/"+doc+"_ar.html");//hange this to your own link
          //window.open("https://docs.google.com/viewer?url=https://www.easywinz.com/documents/"+doc+"_ar.pdf&embedded=true",'_system', 'location=yes');

        }
        else{
          InAppBrowser.create("https://www.easywinz.com/documents/"+doc+"_in.html");//hange this to your own link
          //window.open("https://docs.google.com/viewer?url=https://www.easywinz.com/documents/"+doc+"_in.pdf&embedded=true",'_system', 'location=yes');
        }
  }
}
