import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-shipment',
  templateUrl: './shipment.page.html',
  styleUrls: ['./shipment.page.scss'],
})
export class ShipmentPage implements OnInit {
  traduction: any = {};
  countrys: any = [];
  shippingInfo: any = {};
  constructor(private authSevice: AuthService) { 
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.shippingInfo = {
      "full_name": "",
      "address1": "",
      "address2": "",
      "phone": "",
      "city": "",
      "zip_code": "",
      "country": 1
    }
  }

  ngOnInit() {
    this.authSevice.getAllCountry().subscribe(
      (data: any) => {
        this.countrys = data.Country;
        console.log(data);
      }
    )
  }

}
