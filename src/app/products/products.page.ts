import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/services/user.service';
import { TraductionService } from 'src/services/traduction.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  products: any = [];
  traduction: any = {};
  language: any;
  constructor(private userService: UserService, private traductionService: TraductionService) {
    this.traduction = JSON.parse(localStorage.getItem('traduction'));
    this.language = localStorage.getItem('language');
  }

  ngOnInit() {
    this.userService.getProducts().subscribe(
      (data: any) => {
        this.products = data.Fixed_Product;
      }
    )

  }

}
