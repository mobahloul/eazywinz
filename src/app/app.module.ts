import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TraductionService } from 'src/services/traduction.service';
import { CartService } from 'src/services/cart.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { File } from "@ionic-native/file/ngx";
import { FileTransfer } from "@ionic-native/file-transfer/ngx";
import { PdfViewerService } from 'src/services/pdf-viewer.service';
import { HTTP } from '@ionic-native/http/ngx';
import { Http, HttpModule } from '@angular/http';
import { Stripe } from '@ionic-native/stripe/ngx';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireFunctionsModule, ORIGIN, REGION } from '@angular/fire/functions';
import { OneSignal } from '@ionic-native/onesignal/ngx';

var firebaseConfig = {
  apiKey: "AIzaSyCF4MbuiihIibKVjKgpGufcyNwRdkAh0Co",
  authDomain: "test-84a72.firebaseapp.com",
  projectId: "test-84a72",
  storageBucket: "test-84a72.appspot.com",
  messagingSenderId: "1095535334983",
  appId: "1:1095535334983:web:2535e2abc4a3294b898362",
  measurementId: "G-WQBQS0TK88"
}
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireFunctionsModule,
    AngularFirestoreModule,
    HttpModule,
    ServiceWorkerModule.register('ngsw-worker.js',
      { enabled: environment.production })
  ],
  providers: [
    OneSignal,
    FileTransfer,
    FileOpener,
    File,
    PdfViewerService,
    CartService,
    TraductionService,
    StatusBar,
    SplashScreen,
    HTTP,
    Stripe,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    // { provide: REGION, useValue: 'europe-west6' },
    // { provide: ORIGIN, useValue: 'https://us-central1-test-84a72.cloudfunctions.net' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
