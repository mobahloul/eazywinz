import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TraductionService } from 'src/services/traduction.service';
import { UserService } from 'src/services/user.service';
import { AlertsService } from 'src/services/alerts.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { CartService } from 'src/services/cart.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Headers, Http, RequestOptions } from '@angular/http';
import { AuthService } from 'src/services/auth.service';
import { AngularFireFunctions } from '@angular/fire/functions';
import { HomeService } from 'src/services/home.service';
// import { Stripe } from '@ionic-native/stripe/ngx';
import * as moment from 'moment';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  add: boolean = true;
  card: any;
  traduction: any = {};
  user: any = {};
  loader: boolean = false;
  cards: any = [];
  loading: any;
  years: any = [];
  exp_date_month = 0;
  exp_date_year = 0;
  k = -1;
  check = false;
  products: any = [];
  stringproductid: any = "";
  stringproductquantity: any = "";
  stringproducttickets: any = "";
  stringcartid: any = "";
  total: number = 0;
  currency = "USD";
  language: any;
  headers: Headers = new Headers();
  options: any;

  confirmation: any;

  //////
  paymentamount: string = '33.3'
  currencyIcon: string = "$";
  stripe_key = "pk_live_51Iw32iJvCiXIgZlvNtwWnAwefIJpnMvlah6k8IIDnYz9nfJ4dTb3bZhapOkLjwYzFkbXLdQBerUjyLE3iOh50Nab009nXr4w6c";
  strip_sk = "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"
  cardDetails: any = {};


  cnumber: any = '';
  cname: any = '';
  cvc: any = '';
  date: any = '';
  email: any = '';

  constructor(private router: Router,
    private traductionService: TraductionService,
    private alertsService: AlertsService,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,
    private cartService: CartService,
    public http: HTTP,
    // public nativestripe:Stripe,
    public toastCtrl: ToastController,
    public http2: HttpClient,
    public http3: Http,
    private userService: UserService,
    public auth: AuthService,
    private functions: AngularFireFunctions,
    public home: HomeService
  ) {
    this.headers.append('Authorization', 'Bearer sk_test_XKokBfNWv6FIYuTMg5sLPjhJ');

    this.options = new RequestOptions({ headers: this.headers });
    this.language = localStorage.getItem('language');
    this.currency = localStorage.getItem('currency');
    this.route.params.subscribe(params => {
      if (params['check'] == 1) {
        this.check = true;
      }
    });
    this.traduction = JSON.parse(localStorage.getItem('traduction'));

    this.years = this.generateArrayOfYears(2030);
    console.log(this.years)
  }

  getMaxDate(): string {
    return moment().add('50', 'years').format('YYYY-MM-DD');
  }
  addNewCard() {
    this.cards = [];
  }
  minStartDate(): string {
    return moment().format('YYYY-MM-DD');
  }

  ngOnInit() {
    this.presentLoading();
    this.user = JSON.parse(localStorage.getItem('user'));

    // this.setupStripe();
    this.getCards(this.user.stripe_key);
    // this.cartService.getCart().subscribe(
    //   (data: any) => {
    //     this.products = data.Favorites;
    //     console.log(this.products);
    //     this.products.forEach((element, index) => {

    //     });
    //   }
    // )
    this.getCart();

  }
  getCart() {
    this.cartService.getCart().subscribe(
      (data: any) => {
        console.log(data
        )
        this.products = data.Favorites;
        let nbr = 0;
        data.Favorites.forEach((element, index) => {
          nbr = nbr + Number(element.cart_amount);
          if (index == data.Favorites.length - 1) {
            this.cartService.productsNbr.next(nbr);
          }
        });
        if (this.products.length > 0) {
          this.products.forEach((element, index) => {
            this.stringcartid = this.stringcartid + element.cart_id + ' ';
            this.stringproductid = this.stringproductid + element.product_id + ' ';
            this.stringproductquantity = this.stringproductquantity + element.cart_amount + ' ';
            this.stringproducttickets = this.stringproductquantity;
            //this.total = this.total + Number(element.product_prise);
            this.total = this.total + (Number(element.product_prise) * Number(element.cart_amount));
            if (index == this.products.length - 1) {
              this.hideLoader();
              this.total = this.total * Number(localStorage.getItem('currency_price_for_AED'));
            }
          });
        }
        this.loadingCtrl.dismiss();

      }, err => {
        this.loadingCtrl.dismiss();
      }
    )
  }
  token;

  getCards(stripe_key) {
    // console.log(this.util.userInfo.stripe_key);

    this.home.httpGet('https://api.stripe.com/v1/customers/' + stripe_key +
      '/sources?object=card', this.strip_sk).subscribe((cards: any) => {
        console.log(cards);
        if (cards && cards.data.length > 0) {
          this.cards = cards.data;
          this.token = this.cards[0].id;
          this.card = this.cards[0];
        }
      }, (error) => {
        console.log(error);
        // if (error && error.error && error.error.error && error.error.error.message) {
        //   this.util.showErrorAlert(error.error.error.message);
        //   return false;
        // }
        // this.util.errorToast(this.util.getString('Something went wrong'));
      });
  }

  back() {
    this.router.navigate(['/profile']);
  }
  changeValue(value) {
    console.log(value);

    if (value.length == 2) {
      console.log(value.length)
      //  this.card.exp_date.concat("/");
      console.log(this.card.exp_date)
    }
  }
  send() {
    this.Pay();
    return
    this.user = JSON.parse(localStorage.getItem('user'));
    this.card.user_id = this.user.user_id;
    this.card.exp_date = this.exp_date_year + '-' + this.exp_date_month + '-1';
    if (this.card.card_num.trim() == '' ||
      this.card.name_on_card.trim() == ''
      || this.card.exp_date.trim() == '' ||
      this.card.cvc.trim() == '') {
      this.alertsService.presentToast(this.traduction.allReqFields);
      this.loader = false;
    } else {
      this.presentLoading();

      // this.userService.addCreditCard(this.card).subscribe(
      //   (data: any) => {
      //     if (data.result) {
      //       this.add = false;
      //       this.getCards();
      //       this.hideLoader();
      //       this.card = {
      //         "card_num": "",
      //         "name_on_card": "",
      //         "exp_date": "",
      //         "cvc": "",
      //         "card_type": 0
      //       }
      //       this.exp_date_month = 0;
      //       this.exp_date_year = 0;
      //     } else {
      //       this.alertsService.presentToast('Please use valid card');
      //     }
      //     console.log(data);
      //   }, (err) => {
      //     this.alertsService.presentToast('Error');
      //   }
      // )
    }

  }

  async showToast(msg, colors, positon) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
      color: colors,
      position: positon
    });
    toast.present();
  }
  async Pay() {
    if (this.card.id)
      this.makePayment(this.card.id, this.card.customer);
    return;
  }
  async addCard() {
    if (this.cname === '' || this.cnumber === '' ||
      this.cvc === '' || this.date === '') {
      // this.util.showToast('All Fields are required', 'danger', 'bottom');
      this.showToast('All Fields are required', 'danger', 'bottom');
      return false;
    }
    // const emailfilter = /^[\w._-]+[+]?[\w._-]+@[\w.-]+\.[a-zA-Z]{2,6}$/;
    // if (!(emailfilter.test(this.email))) {
    //   this.showToast('Please enter valid email', 'danger', 'bottom');
    //   return false;
    // }
    this.presentLoaingMsg("Creating Card ...");
    const year = this.date.split('-')[0];
    const month = this.date.split('-')[1];

    const param = {
      'card[number]': this.cnumber,
      'card[exp_month]': month,
      'card[exp_year]': year,
      'card[cvc]': this.cvc,
      'card[customer]': this.user.stripe_key
    };
    this.home.externalPost('https://api.stripe.com/v1/tokens', param, this.strip_sk).subscribe(async (data: any) => {
      console.log(data);
      if (data && data.id) {
        // this.token = data.id;
        const customer = {
          description: 'EasyWinz Customer',
          source: data.id,
          email: this.user.email
        };
        await this.hideLoader();

        // if (!this.user.stripe_key || this.user.stripe_key == "") {
        await this.presentLoaingMsg("Creating Customer ...")
        this.home.externalPost('https://api.stripe.com/v1/customers', customer, this.strip_sk).subscribe((customer: any) => {
          console.log(customer);
          // this.util.hide();
          if (customer && customer.id) {
            // this.cid = customer.id;
            const cid = {
              id: localStorage.getItem('uid'),
              stripe_key: customer.id
            };
            this.user.stripe_key = customer.id
            // this.updateUser(cid);
            this.userService.updateUserDetails(this.user).subscribe(res => {
              localStorage.setItem('user', JSON.stringify(this.user));

              this.getCards(this.user.stripe_key);
            })
            this.makePayment(data.id, this.user.stripe_key)
          }
          this.loadingCtrl.dismiss();
        }, error => {
          this.loadingCtrl.dismiss();
          this.showToast(error.error.error.message, "danger", "bottom");

        });
        // }
      } else {
        this.loadingCtrl.dismiss();
      }
    }, err => {
      this.loadingCtrl.dismiss();
      // let errrormsg= JSON.parse(err);
      this.showToast(err.error.error.message, "danger", "bottom");
      console.log(err.error.error.message);
    })


  }
  async makePayment(token, email) {
    await this.presentLoaingMsg("Saving payments ...");

    let amount = parseInt(((this.total / Number(localStorage.getItem('currency_price_for_AED'))) * 100).toFixed(2));

    const options = {
      amount: amount,
      currency: "AED",
      customer: email,
      card: token,
    };
    console.log('options', options);
    const url = 'https://api.stripe.com/v1/charges';
    // this.util.show();
    this.home.externalPost(url, options, this.strip_sk).subscribe(async (data: any) => {
      console.log('------------------------->', data);
      // this.paykey = data.id;
      await this.hideLoader();
      this.validatePayment(data.id);

    }, async (error) => {
      // this.util.hide();
      await this.hideLoader();
      this.showToast(error.error.error.message, "danger", "bottom");

      console.log(error);
    });
  }
  async presentLoading() {
    await this.loadingCtrl.create({
      message: this.traduction.messageLoading
    }).then((res) => {
      res.present();
    });
  }

  async presentLoaingMsg(msg) {
    await this.loadingCtrl.create({
      message: msg
    }).then((res) => {
      res.present();
    });
  }
  async hideLoader() {
    await this.loadingCtrl.dismiss().then((res) => {
    }).catch((error) => {
    });
  }

  generateArrayOfYears(endYear) {
    var currentYear = new Date().getFullYear(), years = [];
    endYear = endYear || 2030;
    while (currentYear <= endYear) {
      years.push(currentYear++);
    }
    return years;
  }

  async validatePayment(payment_id) {
    //removed cards verification and using buyProduct directly
    //if (this.k > -1) {
    await this.presentLoaingMsg("Completing your order ...");
    let amount = this.total;
    amount = parseInt(((this.total / Number(localStorage.getItem('currency_price_for_AED'))) * 100).toFixed(2));
    let data = {
      user_id: this.user.user_id,
      stringcartid: this.stringcartid.replace(/\s+$/, ''),
      stringproductid: this.stringproductid.replace(/\s+$/, ''),
      stringproductquantity: this.stringproductquantity.replace(/\s+$/, ''),
      stringproducttickets: this.stringproducttickets.replace(/\s+$/, ''),
      transaction_value: amount,
      transaction_id: payment_id
    }
    console.log(data)
    this.cartService.buyProduct(data).subscribe(
      (d: any) => {
        console.log(d)
        if (d.result) {
          setTimeout(
            () => {
              this.hideLoader();
              this.getCart();
              this.router.navigate(['./thanks'], { replaceUrl: true });
            }, 1000
          )
        } else {
          this.hideLoader();

        }
      }, err => {
        console.log(err)
        this.hideLoader();
      }
    )
    // } else {
    //   // this.hideLoader();
    //   this.alertsService.presentToast('please select one of credits cards')
    // }
  }

  /**
   * 
   *  const param = {
      'card[number]': this.cnumber,
      'card[exp_month]': month,
      'card[exp_year]': year,
      'card[cvc]': this.cvc
    };
    this.util.show();
    this.api.externalPost('https://api.stripe.com/v1/tokens', param, this.util.stripe).subscribe((data: any) => {
      console.log(data);
      if (data && data.id) {
        // this.token = data.id;
        const customer = {
          description: 'Customer for food app',
          source: data.id,
          email: this.email
        };
        this.api.externalPost('https://api.stripe.com/v1/customers', customer, this.util.stripe).subscribe((customer: any) => {
          console.log(customer);
          this.util.hide();
          if (customer && customer.id) {
            // this.cid = customer.id;
            const cid = {
              id: localStorage.getItem('uid'),
              stripe_key: customer.id
            };
            this.updateUser(cid);
          }
        }, error => {
          this.util.hide();
          console.log();
          if (error && error.error && error.error.error && error.error.error.message) {
            this.util.showErrorAlert(error.error.error.message);
            return false;
          }
          this.util.errorToast(this.util.getString('Something went wrong'));
        });
      } else {
        this.util.hide();
      }
    }, (error: any) => {
      console.log(error);
      this.util.hide();
      console.log();
      if (error && error.error && error.error.error && error.error.error.message) {
        this.util.showErrorAlert(error.error.error.message);
        return false;
      }
      this.util.errorToast(this.util.getString('Something went wrong'));
    });
  }
   */

}
